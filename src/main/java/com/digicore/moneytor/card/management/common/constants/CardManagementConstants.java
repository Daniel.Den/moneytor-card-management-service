package com.digicore.moneytor.card.management.common.constants;

import org.springframework.beans.factory.annotation.Value;

public class CardManagementConstants {

    public static final String CLIENT_ID = "moneytor-card-management-service";

    public static final String CLIENT_NAME = "moneytor-card-management-service";

    public static final String CLIENT_SECRET = "secret";

    // would need to add other services used
    public final static String RESOURCE_IDS = "aptent-service,aptent-ticketing-service,aptent-notification-service," +
            "aptent-card-service,aptent-account-statement-service," +
            "moneytor-vas-service,cosmos-control-service,cosmos-control-client,olympos-service";

    public final static String SCOPE = "profile";

    public final static String AUTO_APPROVED_SCOPES = "profile";

    public final static String AUTHORIZED_GRANT_TYPES = "implicit,client_credentials,password,authorization_code";

    public final static String AUTHORITIES = "implicit,client_credentials,password";

    public final static Integer ACCESS_TOKEN_VALIDITY_SECONDS = 3600;

    public final static Integer REFRESH_TOKEN_VALIDITY_SECONDS = 3600;
}
