package com.digicore.moneytor.card.management.modules.service;

import com.digicore.moneytor.card.management.modules.apimodel.CardWorkflowApiModel;
import com.google.common.base.Strings;
import com.teamapt.cosmos.control.lib.AuthenticationUtils;
import com.teamapt.exceptions.CosmosServiceException;
import com.teamapt.moneytor.data.modules.card.model.CardRequest;
import com.teamapt.moneytor.data.modules.common.model.AssetType;
import com.teamapt.moneytor.data.modules.corporate.model.CorporateInstitution;
import com.teamapt.moneytor.data.modules.merchants.model.CorporateInstitutionAccount;
import com.teamapt.moneytor.data.modules.merchants.repository.CorporateInstitutionAccountRepository;
import com.teamapt.moneytor.data.modules.transfer.model.ApprovalAction;
import com.teamapt.moneytor.data.modules.transfer.model.Status;
import com.teamapt.moneytor.data.modules.workflow.model.CorporateInstitutionWorkflow;
import com.teamapt.moneytor.data.modules.workflow.model.Workflow;
import com.teamapt.moneytor.data.modules.workflow.model.WorkflowUserTask;
import com.teamapt.moneytor.data.utils.WorkflowUtils;
import com.teamapt.moneytor.lib.audit.MoneytorAuditService;
import com.teamapt.moneytor.lib.common.constants.AuditMessages;
import com.teamapt.moneytor.lib.common.service.AuthenticationService;
import com.teamapt.moneytor.lib.common.util.MoneytorUtils;
import com.teamapt.moneytor.lib.corporateinstitution.CorporateInstitutionService;
import com.teamapt.moneytor.lib.workflow.MoneytorWorkflowService;
import com.teamapt.moneytor.lib.workflow.model.AuthenticatedWorkflowApprovalRequest;
import com.teamapt.workflow.apimodel.WorkflowDecision;
import com.teamapt.workflow.service.IWorkflowService;
import org.flowable.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
public class CardRequestWorkflowService implements IWorkflowService<CardRequest, CardWorkflowApiModel,
        AuthenticatedWorkflowApprovalRequest> {

    @Autowired
    private MoneytorWorkflowService workflowService;

    @Autowired
    private MoneytorAuditService auditService;

    @Autowired
    private CardService cardService;

    @Autowired
    private CorporateInstitutionService corporateInstitutionService;

    @Autowired
    private CorporateInstitutionAccountRepository corporateInstitutionAccountRepository;

    @Autowired
    private AuthenticationService authenticationService;

    /**
     * @param cardRequest
     * @return
     * @throws CosmosServiceException
     */
    @Override
    public CardRequest startWorkflowProcess(CardRequest cardRequest) throws CosmosServiceException {

        CorporateInstitution corporateInstitution = cardRequest.getCorporateInstitution();

        CorporateInstitutionAccount corporateInstitutionAccount = this.corporateInstitutionAccountRepository.
                findOneByCorporateInstitutionIdAndAccountProviderCodeAndAccountNumber(corporateInstitution.getId(),
                        cardRequest.getSourceAccountProviderCode(), cardRequest.getSourceAccountNumber());

        if (corporateInstitutionAccount == null)
            throw new CosmosServiceException("Unable to retrieve account details.");

        String initiator = cardRequest.getInitiator();
        Long cardRequestFee = cardRequest.getCardRequestFee();

        CorporateInstitutionWorkflow corporateInstitutionWorkflow = this.workflowService.getWorkflow(
                corporateInstitutionAccount, AssetType.CARD_REQUEST);

        Workflow workflow = new Workflow();
        workflow.setInitiator(initiator);
        workflow.setTimeStarted(LocalDateTime.now());
        workflow.setCorporateInstitution(corporateInstitution);
        workflow.setName("Card request made by " + cardRequest.getTransactionName());
        workflow.setCorporateInstitutionWorkflow(corporateInstitutionWorkflow);
        workflow.setInstanceId(cardRequest.getId());
        workflow.setWorkflowType(AssetType.CARD_REQUEST);

        if (corporateInstitutionWorkflow == null || Strings.isNullOrEmpty(corporateInstitutionWorkflow.
                getUserTaskUserMapping())) {
            workflow.setNextUsers(null);
            workflow.setStatus(Status.APPROVED);
            workflow.setNextUserTasks(null);
            workflow.setTerminator(initiator);
            workflow.setTimeEnded(LocalDateTime.now());
            workflow = this.workflowService.save(workflow);

            this.auditService.save(AuthenticationUtils.getPrincipal(), workflow, AuditMessages.INITIATE_CARD_REQUEST,
                    "User successfully initiated the card request workflow.",
                    AuthenticationUtils.getPrincipalAuthenticationDomainId(false));

            this.cardService.updateCardRequestStatus(corporateInstitution, cardRequest.getId(), Status.APPROVED);

            return this.executeOperation(cardRequest);
        }

        try {
            Map<String, Object> variableMap = new HashMap<>();
            variableMap.put("amount", ((double) cardRequestFee) / 100);

            //trigger workflow
            List<Task> tasks = this.workflowService.initiateWorkflow(
                    corporateInstitutionWorkflow.getProcessDefinitionId(), variableMap);

            workflow.setProcessInstanceId(tasks.get(0).getProcessInstanceId());
            workflow.setStatus(Status.PENDING_APPROVAL);
            workflow.setNextUserTasks(MoneytorUtils.objectToString(MoneytorWorkflowService.
                    getNextUserTaskMap(corporateInstitutionWorkflow.getUserTaskUserMap(), tasks)));
            workflow.setNextUsers(this.workflowService.getDelimitedNextUsersConsideringHistory(
                    workflow.getProcessInstanceId(), corporateInstitutionWorkflow.getUserTaskUserMap(), tasks));

            this.auditService.save(AuthenticationUtils.getPrincipal(), workflow, AuditMessages.INITIATE_CARD_REQUEST,
                    "User successfully initiated the card request workflow.",
                    AuthenticationUtils.getPrincipalAuthenticationDomainId(false));
        } catch (Exception ex) {
            workflow.setNextUsers(null);
            workflow.setNextUserTasks(null);
            workflow.setProcessInstanceId(null);
            workflow.setStatus(Status.FAILED);
            workflow.setTimeEnded(LocalDateTime.now());

            this.auditService.save(AuthenticationUtils.getPrincipal(), workflow, AuditMessages.INITIATE_CARD_REQUEST,
                    "User tried to initiate the card request workflow and failed.",
                    AuthenticationUtils.getPrincipalAuthenticationDomainId(false));

            this.cardService.updateCardRequestStatus(corporateInstitution, workflow.getInstanceId(), Status.FAILED);

            throw new CosmosServiceException("An error occurred while trying to initiate workflow");
        } finally {
            this.workflowService.save(workflow);
        }

        return cardRequest;
    }

    /***
     *
     * @param principal
     * @param workflowApprovalRequest
     * @return
     * @throws CosmosServiceException
     */
    @Override
    public synchronized CardWorkflowApiModel approvalRequest(Principal principal,
                                                             AuthenticatedWorkflowApprovalRequest
                                                                     workflowApprovalRequest)
            throws CosmosServiceException {

        ApprovalAction approvalAction;

        // Get workflow workflowDecision
        WorkflowDecision workflowDecision = workflowApprovalRequest.getDecision();

        List<Long> approvedCardRequestIds = new ArrayList<>();

        // Get corporate institution
        CorporateInstitution corporateInstitution = this.corporateInstitutionService.
                getCorporateInstitution(principal);

        // Get workflow
        Workflow workflow = this.workflowService.getWorkflowById(corporateInstitution.getId(),
                workflowApprovalRequest.getId(), AssetType.CARD_REQUEST);

        // Check if workflow(card request) is available for approval process
        if (workflow == null || Strings.isNullOrEmpty(workflow.getProcessInstanceId()) || workflow.getStatus()
                .equals(Status.APPROVED) || workflow.getStatus().equals(Status.DECLINED))
            // Throw error if workflow(card request) is not available for approval process
            throw new CosmosServiceException("This card request is not available for approval process");

        // Get the card request
        CardRequest cardRequest = this.cardService.getCardRequest(workflow.getInstanceId(),
                corporateInstitution.getId());

        // Authenticate card request
        this.authenticationService.authenticateRequest(principal, cardRequest.getSourceAccountNumber(),
                AuthenticatedWorkflowApprovalRequest.toAuthenticationRequest(workflowApprovalRequest));

        // Check if workflow decision is DECLINE_WITH_FILTER
        if (workflowDecision.equals(WorkflowDecision.DECLINE_WITH_FILTER))
            //  Returns the id of an approved card request
            approvedCardRequestIds = this.cardService.cardRequestNotInFilteredList(corporateInstitution,
                    workflow.getInstanceId(), workflowApprovalRequest.getFilteredIds());

        // Get corporate institution workflow
        CorporateInstitutionWorkflow corporateInstitutionWorkflow = workflow.getCorporateInstitutionWorkflow();

        // Get the username of the logged-in user
        String username = AuthenticationUtils.getPrincipalUser(principal).getUsername();

        WorkflowUserTask task = this.workflowService.validateUserAuthorityInWorkflowTask(username, workflow);

        if (workflowDecision.equals(WorkflowDecision.DECLINE) ||
                (workflowDecision.equals(WorkflowDecision.APPROVE_WITH_FILTER) &&
                        (workflowApprovalRequest.getFilteredIds() == null ||
                        workflowApprovalRequest.getFilteredIds().isEmpty())))
            // Terminate workflow if the above check is not met
            return this.terminateWorkflow(principal, workflow.getInstanceId(), workflowApprovalRequest);

        approvalAction = new ApprovalAction(username, true, workflowApprovalRequest);

        Map<String, ApprovalAction> approvalActionMap = WorkflowUtils.getProgressMap(workflow.getProgress());
        approvalActionMap.put(task.getTaskDefinitionKey(), approvalAction);

        // Set the progress of a workflow
        workflow.setProgress(MoneytorUtils.objectToString(approvalActionMap));

        this.workflowService.claimAndCompleteUserTask(username, task.getTaskId(), null);

        // Check if workflow has ended
        if (this.workflowService.processInstanceEnded(workflow.getProcessInstanceId())) {
            workflow.setNextUserTasks(null);
            workflow.setStatus(Status.APPROVED); // Set workflow status to approved
            workflow.setTimeEnded(LocalDateTime.now());
            workflow.setNextUsers(null);
            workflow.setTerminator(username);

            // Check if workflow decision is APPROVE_WITH_FILTER
            if (workflowDecision.equals(WorkflowDecision.APPROVE_WITH_FILTER))
                this.cardService.finalCardRequestApproval(corporateInstitution, workflow.getInstanceId(),
                        workflowApprovalRequest.getFilteredIds());
            // Check if workflow decision is DECLINE_WITH_FILTER
            else if (workflowDecision.equals(WorkflowDecision.DECLINE_WITH_FILTER))
                this.cardService.finalCardRequestApproval(corporateInstitution, workflow.getInstanceId(),
                        approvedCardRequestIds);
            else
                // Update card request workflow status to APPROVED
                this.cardService.updateCardRequestStatus(corporateInstitution, workflow.getInstanceId(),
                        Status.APPROVED);

            workflow.setWorkflowEnded(true);
            workflow.setTargetObject(cardRequest);

            this.auditService.save(principal, workflow, AuditMessages.APPROVE_CARD_REQUEST_WORKFLOW,
                    "User successfully approved a card request workflow to completion.");

            // Save workflow
            this.workflowService.save(workflow);

            this.executeOperation(cardRequest);

            return new CardWorkflowApiModel(cardRequest);
        }

        // Get list of tasks for a workflow
        List<Task> tasks = this.workflowService.getNextTasks(workflow.getProcessInstanceId());

        // Set next user tasks on workflow
        workflow.setNextUserTasks(MoneytorUtils.objectToString(MoneytorWorkflowService.getNextUserTaskMap(
                corporateInstitutionWorkflow.getUserTaskUserMap(), tasks)));
       // Set next users on workflow
        workflow.setNextUsers(this.workflowService.getDelimitedNextUsersConsideringHistory(
                workflow.getProcessInstanceId(), corporateInstitutionWorkflow.getUserTaskUserMap(), tasks));

        // Check if workflow decision is APPROVE_WITH_FILTER
        if (workflowDecision.equals(WorkflowDecision.APPROVE_WITH_FILTER)) {
            this.cardService.declineCardRequestNotInList(principal,
                    workflow.getInstanceId(), workflowApprovalRequest.getFilteredIds());
            // Check if workflow decision is DECLINE_WITH_FILTER
        } else if (workflowDecision.equals(WorkflowDecision.DECLINE_WITH_FILTER)) {
            this.cardService.declineCardRequestNotInList(principal, workflow.getInstanceId(), approvedCardRequestIds);
        }

        this.auditService.save(principal, workflow, AuditMessages.APPROVE_CARD_REQUEST_WORKFLOW,
                "User successfully approved a card request workflow.");

        // Save workflow
        this.workflowService.save(workflow);

        return new CardWorkflowApiModel(cardRequest);
    }

    /**
     * @param principal
     * @param cardRequestId
     * @param request
     * @return
     * @throws CosmosServiceException
     */
    @Override
    public synchronized CardWorkflowApiModel terminateWorkflow(Principal principal, Long cardRequestId,
                                                               AuthenticatedWorkflowApprovalRequest request)
            throws CosmosServiceException {

        CorporateInstitution corporateInstitution = this.corporateInstitutionService.getCorporateInstitution(principal);

        String username = AuthenticationUtils.getPrincipalUser(principal).getUsername();

        Workflow workflow = this.workflowService.getWorkflowByInstanceId(corporateInstitution.getId(),
                cardRequestId, AssetType.CARD_REQUEST);

        if (workflow == null || workflow.getProcessInstanceId() == null)
            return null;

        String comment = request == null ? null : request.getComment();

        this.workflowService.terminateProcessInstance(workflow.getProcessInstanceId(), comment);

        WorkflowUserTask task = this.workflowService.validateUserAuthorityInWorkflowTask(username, workflow);

        ApprovalAction action = new ApprovalAction(username, false, request);

        Map<String, ApprovalAction> approvalActionMap = WorkflowUtils.getProgressMap(workflow.getProgress());
        approvalActionMap.put(task.getTaskDefinitionKey(), action);

        workflow.setProgress(MoneytorUtils.objectToString(approvalActionMap));
        workflow.setStatus(Status.DECLINED);
        workflow.setTimeEnded(LocalDateTime.now());
        workflow.setNextUserTasks(null);
        workflow.setNextUsers(null);
        workflow.setTerminator(username);

        this.cardService.updateCardRequestStatus(corporateInstitution, workflow.getInstanceId(), Status.DECLINED);

        this.auditService.save(principal, workflow, AuditMessages.TERMINATE_CARD_REQUEST_WORKFLOW,
                "User successfully terminated an card request workflow.");

        this.workflowService.save(workflow);

        return new CardWorkflowApiModel(workflow.getStatus(), null);
    }

    /**
     * Processes a card request
     *
     * @param cardRequest
     * @return
     * @throws CosmosServiceException
     */
    @Override
    public CardRequest executeOperation(CardRequest cardRequest) throws CosmosServiceException {
        return this.cardService.processCardRequest(cardRequest);
    }
}
