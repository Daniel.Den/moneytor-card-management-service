package com.digicore.moneytor.card.management.modules.apimodel;

import com.teamapt.moneytor.data.modules.card.enums.CardRequestStatus;
import com.teamapt.moneytor.data.modules.card.enums.CardRequestType;
import com.teamapt.moneytor.data.modules.card.enums.CardType;
import com.teamapt.moneytor.lib.common.model.ValidationStatus;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.util.List;

public class CardRequest {

    @NotEmpty
    @NotNull
    private String customerId;

    private boolean isPriceFixed;

    @NotEmpty
    @NotNull
    private Long cardRequestFee;

    private CardType cardType;

    private CardRequestType cardRequestType;

    private CardRequestStatus cardRequestStatus;

    private ValidationStatus validationStatus;

    private List<String> validationMessage;


    public CardRequest() {
        this.isPriceFixed = true;
        this.cardType = CardType.DEBIT;
        this.cardRequestStatus = CardRequestStatus.REQUEST_REVIEW;
    }

    public CardRequest(String customerId, CardRequestType cardRequestType) {
        this.customerId = customerId;
        this.cardRequestType = cardRequestType;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public boolean isPriceFixed() {
        return isPriceFixed;
    }

    public void setPriceFixed(boolean priceFixed) {
        isPriceFixed = priceFixed;
    }

    public CardType getCardType() {
        return cardType;
    }

    public void setCardType(CardType cardType) {
        this.cardType = cardType;
    }

    public CardRequestType getCardRequestType() {
        return cardRequestType;
    }

    public void setCardRequestType(CardRequestType cardRequestType) {
        this.cardRequestType = cardRequestType;
    }

    public CardRequestStatus getCardRequestStatus() {
        return cardRequestStatus;
    }

    public void setCardRequestStatus(CardRequestStatus cardRequestStatus) {
        this.cardRequestStatus = cardRequestStatus;
    }

    public Long getCardRequestFee() {
        return cardRequestFee;
    }

    public void setCardRequestFee(Long cardRequestFee) {
        this.cardRequestFee = cardRequestFee;
    }

    public ValidationStatus getValidationStatus() {
        return validationStatus;
    }

    public void setValidationStatus(ValidationStatus validationStatus) {
        this.validationStatus = validationStatus;
    }

    public List<String> getValidationMessage() {
        return validationMessage;
    }

    public void setValidationMessage(List<String> validationMessage) {
        this.validationMessage = validationMessage;
    }
}
