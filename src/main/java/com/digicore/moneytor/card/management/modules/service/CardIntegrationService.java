package com.digicore.moneytor.card.management.modules.service;

import com.teamapt.aptent.vas.service.lib.responses.VasServiceResponse;
import com.teamapt.exceptions.BadRequestApiException;
import com.teamapt.exceptions.CosmosServiceException;
import com.teamapt.moneytor.data.modules.card.model.CardRequest;
import com.teamapt.moneytor.data.modules.corporate.model.CorporateInstitution;
import com.teamapt.moneytor.data.modules.transfer.model.Status;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResourceAccessException;

import java.time.LocalDateTime;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 */
@Component
public class CardIntegrationService {

    private final Logger logger = Logger.getLogger(CardIntegrationService.class.getName());

    /**
     *
     * @param cardRequest
     * @return
     * @throws CosmosServiceException
     */
    public CardRequest processCardRequest(CardRequest cardRequest) throws CosmosServiceException {

        /*
         * Can't implement this until aptent configures a service
         */

        /*CorporateInstitution corporateInstitution = cardRequest.getCorporateInstitution();

        VasServiceResponse vasServiceResponse = null;

        *//*
          todo check for cases
          1. Has an active card?
          2. Request already made for a card?
         *//* // View checks

        String accountName = corporateInstitution.getName();

        *//*BillPaymentRequest billPaymentRequest = generateBillPayment(
                corporateInstitutionAirtime.getSourceAccountNumber(), corporateInstitutionAirtime
                        .getSourceAccountProviderCode(), accountName, corporateInstitution.getEmail(), airtime);*//*

        try {
            *//*
             * Code pertaining to processing a card request
             *//*

            *//*vasServiceResponse = aptentVasService.processSingleBillRequest(corporateInstitutionAirtime.
                            getSourceAccountProviderCode(), corporateInstitutionAirtime.getBatchKey(),
                    corporateInstitutionAirtime.getClientId(), VasServiceRequestType.AIRTIME_RECHARGE,
                    billPaymentRequest);*//*

        } catch (BadRequestApiException exception) {
            logger.log(Level.WARNING, exception.getMessage(), exception);

            cardRequest.setOperationStatus(Status.FAILED);
            cardRequest.setCompletedOn(LocalDateTime.now());
            cardRequest.setResponseCode("BAD_REQUEST");
            cardRequest.setComment("Bad request");
        } catch (ResourceAccessException exception) {
            logger.log(Level.WARNING, exception.getMessage(), exception);

            cardRequest.setOperationStatus(Status.FAILED);
            cardRequest.setComment("Unable to access resource");
            cardRequest.setResponseCode("UNREACHABLE_RESOURCE");
            cardRequest.setCompletedOn(LocalDateTime.now());
        } catch (CosmosServiceException exception) {
            logger.log(Level.WARNING, exception.getMessage(), exception);
            cardRequest.setOperationStatus(Status.PENDING);
        } finally {
            *//*
             * Code pertaining to processing a card request
             *//*

            *//*if (vasServiceResponse == null && (corporateInstitutionAirtime.getTransactionStatus() == null ||
                    corporateInstitutionAirtime.getTransactionStatus().equals(Status.PENDING))) {
                vasServiceResponse = airtimeEnquiry(corporateInstitutionAirtime.getClientId(),
                        corporateInstitutionAirtime.getBatchKey(), airtime.getTransactionId());
            }

            if (vasServiceResponse != null && (corporateInstitutionAirtime.getTransactionStatus() == null ||
                    corporateInstitutionAirtime.getTransactionStatus().equals(Status.PENDING))) {
                corporateInstitutionAirtime = processSingleAirtime(vasServiceResponse, corporateInstitutionAirtime);
            }
            corporateInstitutionAirtimeRepository.save(corporateInstitutionAirtime);*//*
        }

        if (cardRequest.getOperationStatus().equals(Status.FAILED)) {
            throw new CosmosServiceException("Card request operation failed");
        }

        return cardRequest;*/

        return null;
    }
}
