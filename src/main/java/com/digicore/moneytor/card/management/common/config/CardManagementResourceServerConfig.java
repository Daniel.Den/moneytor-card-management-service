package com.digicore.moneytor.card.management.common.config;

import com.teamapt.exceptions.CosmosServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationManager;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
import org.springframework.security.oauth2.provider.token.store.redis.RedisTokenStore;
import org.springframework.stereotype.Component;
import redis.clients.jedis.JedisShardInfo;

import javax.sql.DataSource;

@Component
@EnableResourceServer
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class CardManagementResourceServerConfig extends ResourceServerConfigurerAdapter {

    @Value("${moneytor.card.management.oauth.resource.id}")
    private String resourceId;

    @Value("${cosmos.redis.host}")
    private String redisHost;

    @Value("${cosmos.redis.port}")
    private String redisPort;

    @Value("${cosmos.token.store.option:redis}")
    private String tokenStoreOption;

    @Autowired(required = false)
    @Qualifier("cosmosDatabaseConnection")
    private DataSource cosmosDatasource;

    @Value("#{T(java.lang.Long).parseLong('${spring.cache.redis.time-to-live:600000}')}")
    private Long cacheEvict;

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        resources.resourceId(resourceId)
                .authenticationManager(new OAuth2AuthenticationManager())
                .tokenStore(tokenStore());
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.formLogin().
                loginPage("/login").and().authorizeRequests()
                .antMatchers(new String[]{
//                        "/api/v1/get-user-accounts"
                }).permitAll()
                .anyRequest().authenticated().and()
                .csrf().disable();
    }

    @Bean
    public TokenStore tokenStore() throws CosmosServiceException {
        if ("jdbc".equalsIgnoreCase(tokenStoreOption)) {
            if (cosmosDatasource == null)
                throw new CosmosServiceException("Cosmos datasource not configured");

            return new JdbcTokenStore(cosmosDatasource);
        } else {
            return new RedisTokenStore(moneytorJedisConnectionFactory());
        }
    }

    @Bean
    @Primary
    public JedisConnectionFactory moneytorJedisConnectionFactory() {
        JedisConnectionFactory jedisConnectionFactory = new JedisConnectionFactory();
        jedisConnectionFactory.setHostName(redisHost);
        jedisConnectionFactory.setPort(Integer.parseInt(redisPort));
        jedisConnectionFactory.setShardInfo(new JedisShardInfo(redisHost, redisPort));
        return jedisConnectionFactory;
    }

    @Bean
    RedisTemplate<Object, Object> redisTemplate() {
        RedisTemplate<Object, Object> redisTemplate = new RedisTemplate<Object, Object>();
        redisTemplate.setConnectionFactory(moneytorJedisConnectionFactory());
        return redisTemplate;
    }

    @Bean
    @Primary
    CacheManager cacheManager() {
        RedisCacheManager cacheManager = new RedisCacheManager(redisTemplate());
        cacheManager.setDefaultExpiration(cacheEvict);
        return cacheManager;
    }

}
