package com.digicore.moneytor.card.management.common.constants;

public class ErrorMessages {

    public static final String INVALID_CARD_REQUEST_FEE = "Invalid card request fee";
    public static final String UNRECOGNIZED_CORPORATE_ACCOUNT = "Unrecognized account";
}
