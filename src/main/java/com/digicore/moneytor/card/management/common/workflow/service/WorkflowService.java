package com.digicore.moneytor.card.management.common.workflow.service;

import com.teamapt.exceptions.CosmosServiceException;
import com.teamapt.moneytor.data.modules.common.model.AssetType;
import com.teamapt.moneytor.data.modules.transfer.model.Status;
import com.teamapt.moneytor.lib.common.service.IAsset;
import com.teamapt.moneytor.lib.workflow.model.AuthenticatedWorkflowApprovalRequest;
import com.teamapt.workflow.service.IWorkflowService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.Map;

/**
 *
 */
@Service
public class WorkflowService {

    @javax.annotation.Resource(name = "workflowApprovalMap")
    public Map<AssetType, IWorkflowService> workflowApprovalMap;
    @javax.annotation.Resource(name = "assetMap")
    public Map<AssetType, IAsset> assetMap;

    public Object approvalRequest(Principal principal, AuthenticatedWorkflowApprovalRequest request)
            throws CosmosServiceException {

        IWorkflowService approval = workflowApprovalMap.get(request.getWorkflowType());

        if (approval == null)
            throw new CosmosServiceException("No qualifying handler of approval request");

        return approval.approvalRequest(principal, request);
    }

    public Object getWorkflowInstance(Principal principal, AssetType assetType, Long instanceId)
            throws CosmosServiceException {

        IAsset approval = assetMap.get(assetType);

        if (approval == null)
            throw new CosmosServiceException("No qualifying handler of workflow details request");

        return approval.getAssetDetails(principal, instanceId);
    }

    public Page getWorkflowInstanceDetails(Principal principal, AssetType assetType, Long instanceId,
                                           Status status, String searchParam,
                                           int page, int pageSize) throws CosmosServiceException {
        IAsset approval = assetMap.get(assetType);

        if (approval == null)
            throw new CosmosServiceException("No qualifying handler of workflow details request");

        return approval.getAssetRequests(principal, instanceId, status, searchParam, page, pageSize);
    }
}
