package com.digicore.moneytor.card.management.common.service;

import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.teamapt.exceptions.CosmosServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;

import java.util.UUID;

public class MoneytorRedisStore {

    @Value("#{T(java.lang.Integer).parseInt('${redis.store.expiry:5000}')}")
    private Integer expiryInSeconds;

    @Autowired
    private JedisConnectionFactory connectionFactory;
    private final Gson gson = new Gson();

    public static String generateTransactionKey(String prefix) {
        return prefix + UUID.randomUUID().toString().replace("-", "");
    }

    public void save(String key, String value) {
        save(key, value, expiryInSeconds);
    }

    public void save(String key, String value, int expiry) {
        RedisConnection connection = null;
        try {
            connection = connectionFactory.getConnection();
            byte[] keyBytes = key.getBytes();
            byte[] valueBytes = value.getBytes();
            connection.set(keyBytes, valueBytes);
            connection.expire(keyBytes, expiry);
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }

    public String get(String key) {
        RedisConnection connection = null;
        try {
            connection = connectionFactory.getConnection();
            byte[] keyBytes = key.getBytes();
            byte[] valueBytes = connection.get(keyBytes);
            return new String(valueBytes);
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }

    public <T> T getObject(String key, Class<T> tClass) throws CosmosServiceException {
        try {
            String transaction = get(key);

            if (Strings.isNullOrEmpty(transaction)) {
                throw new CosmosServiceException("Transaction key invalid or expired.");
            }

            return gson.fromJson(transaction, tClass);
        } catch (CosmosServiceException ex) {
            throw new CosmosServiceException(ex);
        }
    }

    public void remove(String key) {
        RedisConnection connection = null;
        try {
            connection = connectionFactory.getConnection();
            byte[] keyBytes = key.getBytes();
            connection.del(keyBytes);
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }
}
