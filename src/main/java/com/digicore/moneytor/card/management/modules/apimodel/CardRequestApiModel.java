package com.digicore.moneytor.card.management.modules.apimodel;

import com.teamapt.moneytor.data.modules.transfer.model.PaymentType;
import com.teamapt.moneytor.lib.account.model.Transfer;

import javax.validation.Valid;

public class CardRequestApiModel extends Transfer {

    @Valid
    private CardRequest cardRequest;

    private String currencyCode;

    private PaymentType paymentType = PaymentType.ONE_TIME;

    public CardRequest getCardRequest() {
        return cardRequest;
    }

    public void setCardRequest(CardRequest cardRequest) {
        this.cardRequest = cardRequest;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    @Override
    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    @Override
    public PaymentType getPaymentType() {
        return paymentType;
    }
}