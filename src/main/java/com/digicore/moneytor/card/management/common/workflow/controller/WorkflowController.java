package com.digicore.moneytor.card.management.common.workflow.controller;

import com.digicore.moneytor.card.management.common.workflow.service.WorkflowService;
import com.teamapt.exceptions.ApiException;
import com.teamapt.moneytor.data.modules.common.model.AssetType;
import com.teamapt.moneytor.data.modules.transfer.model.Status;
import com.teamapt.moneytor.lib.common.controlleradvice.UnprocessableEntityException;
import com.teamapt.moneytor.lib.workflow.model.AuthenticatedWorkflowApprovalRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

/**
 *
 */
@RestController
@RequestMapping("/api/v1/workflow")
public class WorkflowController {

    @Autowired
    private WorkflowService workflowService;

    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    public Object approvalRequest(@Validated @RequestBody AuthenticatedWorkflowApprovalRequest model,
                                  Principal principal) throws UnprocessableEntityException {

        try {
            return workflowService.approvalRequest(principal, model);
        } catch (Exception ex) {
            throw new UnprocessableEntityException(ex);
        }
    }

    @RequestMapping(value = "/details/{workflowType}/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Object getWorkflowInstance(Principal principal, @PathVariable("workflowType") AssetType assetType,
                                      @PathVariable("id") Long instanceId) throws ApiException {

        try {
            return workflowService.getWorkflowInstance(principal, assetType, instanceId);
        } catch (Exception ex) {
            throw new ApiException(ex);
        }
    }

    @RequestMapping(value = "/instance-details/{workflowType}/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Page getWorkflowInstanceDetails(@PathVariable("id") Long instanceId, @PathVariable("workflowType")
            AssetType assetType, @RequestParam(name = "status", required = false) Status status,
                                           @RequestParam("searchParam") String searchParam,
                                           @RequestParam("page") int page, @RequestParam("pageSize") int pageSize,
                                           Principal principal) throws ApiException {

        try {
            return workflowService.getWorkflowInstanceDetails(principal, assetType,  instanceId, status, searchParam,
                    page, pageSize);
        } catch (Exception ex) {
            throw new ApiException(ex);
        }
    }
}
