package com.digicore.moneytor.card.management.modules.controller;

import com.digicore.moneytor.card.management.modules.apimodel.CardWorkflowApiModel;
import com.digicore.moneytor.card.management.modules.apimodel.CardRequestApiModel;
import com.digicore.moneytor.card.management.modules.service.CardService;
import com.teamapt.moneytor.data.modules.card.model.Card;
import com.teamapt.moneytor.lib.common.controlleradvice.UnprocessableEntityException;
import com.teamapt.moneytor.lib.common.util.MoneytorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.List;
import java.util.logging.Logger;

/**
 *
 */
@RestController
@RequestMapping("api/v1/cards")
public class CardController {

    private final Logger logger = Logger.getLogger(CardController.class.getName());

    @Autowired
    private CardService cardService;

    /**
     *
     * @param active
     * @return
     * @throws UnprocessableEntityException
     */
    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseBody
    public List<Card> findAll(@RequestParam(value = "true") boolean active, Principal principal)
            throws UnprocessableEntityException {

        try {
            return this.cardService.findAll(principal, active);
        } catch (Exception exception) {
            logger.info("Card management controller " + exception.getMessage());
            exception.printStackTrace();
            throw new UnprocessableEntityException(exception);
        }
    }

    /**
     * Card request controller method
     *
     * @param cardRequestApiModel
     * @param clientId
     * @param deviceId
     * @param principal
     * @param servletRequest
     * @return
     * @throws UnprocessableEntityException
     */
    @RequestMapping(value = "/request", method = RequestMethod.POST)
    @ResponseBody
    public CardWorkflowApiModel initiateCardRequest(@Validated @RequestBody CardRequestApiModel cardRequestApiModel,
                                                    @RequestHeader(value = "client-id") String clientId,
                                                    @RequestHeader(value = "device-id", required = false)
                                                            String deviceId, Principal principal,
                                                    HttpServletRequest servletRequest)
            throws UnprocessableEntityException {

        try {
            String ipAddress = MoneytorUtils.getRequestIp(servletRequest); // Get ip address

            cardRequestApiModel.setOriginIpAddress(ipAddress); // Set the origin ip address
            cardRequestApiModel.setClientId(clientId); // Set the client id
            cardRequestApiModel.setDeviceId(deviceId); // Set the device id

            return this.cardService.initiateCardRequest(principal, cardRequestApiModel);
        } catch (Exception exception) {
            logger.info("Card management controller " + exception.getMessage());
            exception.printStackTrace();
            throw new UnprocessableEntityException(exception);
        }
    }
}
