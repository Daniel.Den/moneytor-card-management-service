package com.digicore.moneytor.card.management.modules.apimodel;

import com.teamapt.cosmos.control.lib.models.AuthenticationType;
import com.teamapt.moneytor.data.modules.card.model.CardRequest;
import com.teamapt.moneytor.data.modules.transfer.model.Status;
import com.teamapt.moneytor.lib.workflow.model.WorkflowApiModel;
import com.teamapt.moneytor.data.modules.card.model.Card;

/**
 *
 */
public class CardWorkflowApiModel extends WorkflowApiModel {

    private String transactionId;
    private AuthenticationType nextAuthenticationType;

    public CardWorkflowApiModel(CardRequest cardRequest) {
        super(cardRequest.getWorkflowStatus(), cardRequest.getOperationStatus());
    }

    public CardWorkflowApiModel(Status workflowStatus, Status operationStatus) {
        super(workflowStatus, operationStatus);
    }

    public CardWorkflowApiModel(Status workflowStatus, Status operationStatus, String transactionId,
                                AuthenticationType nextAuthenticationType) {
        super(workflowStatus, operationStatus);
        this.transactionId = transactionId;
        this.nextAuthenticationType = nextAuthenticationType;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public AuthenticationType getNextAuthenticationType() {
        return nextAuthenticationType;
    }

    public void setNextAuthenticationType(AuthenticationType nextAuthenticationType) {
        this.nextAuthenticationType = nextAuthenticationType;
    }
}
