package com.digicore.moneytor.card.management.modules.utilities;

import com.teamapt.exceptions.CosmosServiceException;
import com.teamapt.moneytor.data.modules.merchants.model.CorporateInstitutionAccount;
import com.teamapt.moneytor.lib.account.service.MoneytorAccountService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;
import java.util.logging.Logger;

/**
 *
 */
@Service
public class CardUtil {

    private static final String NULL_VALUE = "NULL";

    private static final Logger logger = Logger.getLogger(CardUtil.class.getName());

    @Value("#{'${moneytor.debitable.restriction.codes:kiakia}'.split(',')}")
    private List<String> debitableRestrictionCodes;

    @Autowired
    private MoneytorAccountService accountService;

    public CardUtil() {
    }

    public CardUtil(List<String> debitableRestrictionCodes) {
        this.debitableRestrictionCodes = debitableRestrictionCodes;
    }

    /**
     * This method gets a corporate institution account
     *
     * @param principal
     * @param accountProviderCode
     * @param accountNumber
     * @return
     * @throws CosmosServiceException
     */
    public CorporateInstitutionAccount getCorporateInstitutionAccount(Principal principal, String accountProviderCode,
                                                  String accountNumber) throws CosmosServiceException {

        // Get corporate institution account
        CorporateInstitutionAccount corporateInstitutionAccount = accountService.getCorporateAccount(principal,
                accountProviderCode, accountNumber);

        // Check if corporate institution account is null
        if (corporateInstitutionAccount == null) {
            // Throw exception if corporate institution account is null
            throw new CosmosServiceException("Unable to fetch account details");
        }

        // Check if corporate institution can be debited
        if (!isDebitableAccout(corporateInstitutionAccount)) {
            logger.info("The account selected for transaction cannot be debited: " + accountNumber);
            // Throw exception if corporate institution account can't be debited
            throw new CosmosServiceException("Sorry, this transaction cannot be completed at the moment. " +
                    "Please contact our One Customer Center");
        }

        return corporateInstitutionAccount;
    }

    /**
     * Method checks if a corporate institution account can be debited
     *
     * @param corporateInstitutionAccount
     * @return
     */
    private boolean isDebitableAccout(CorporateInstitutionAccount corporateInstitutionAccount) {
        return StringUtils.isBlank(corporateInstitutionAccount.getRestrictionCode()) ||
                StringUtils.equalsIgnoreCase(NULL_VALUE, corporateInstitutionAccount.getRestrictionCode()) ||
                debitableRestrictionCodes.contains(corporateInstitutionAccount.getRestrictionCode());
    }
}
