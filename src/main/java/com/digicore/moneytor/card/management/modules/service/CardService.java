package com.digicore.moneytor.card.management.modules.service;

import com.digicore.moneytor.card.management.common.constants.ErrorMessages;
import com.digicore.moneytor.card.management.common.service.MoneytorRedisStore;
import com.digicore.moneytor.card.management.modules.apimodel.CardRequestApiModel;
import com.digicore.moneytor.card.management.modules.apimodel.CardWorkflowApiModel;
import com.digicore.moneytor.card.management.modules.utilities.CardUtil;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.teamapt.cosmos.control.lib.AuthenticationUtils;
import com.teamapt.cosmos.control.lib.models.AuthenticationType;
import com.teamapt.cosmos.control.lib.models.CosmosUser;
import com.teamapt.exceptions.CosmosServiceException;
import com.teamapt.moneytor.data.modules.card.enums.CardRequestStatus;
import com.teamapt.moneytor.data.modules.card.enums.CardStatus;
import com.teamapt.moneytor.data.modules.card.enums.CardType;
import com.teamapt.moneytor.data.modules.card.model.Card;
import com.teamapt.moneytor.data.modules.card.model.CardRequest;
import com.teamapt.moneytor.data.modules.card.repository.CardRepository;
import com.teamapt.moneytor.data.modules.card.repository.CardRequestRepository;
import com.teamapt.moneytor.data.modules.corporate.model.CorporateInstitution;
import com.teamapt.moneytor.data.modules.corporate.repository.CorporateInstitutionRepository;
import com.teamapt.moneytor.data.modules.merchants.model.CorporateInstitutionAccount;
import com.teamapt.moneytor.data.modules.systemconfigurations.enums.ConfigurationKey;
import com.teamapt.moneytor.data.modules.systemconfigurations.model.SystemConfiguration;
import com.teamapt.moneytor.data.modules.systemconfigurations.service.SystemConfigurationService;
import com.teamapt.moneytor.data.modules.transfer.model.Status;
import com.teamapt.moneytor.lib.account.service.MoneytorAccountService;
import com.teamapt.moneytor.lib.common.service.AuthenticationService;
import com.teamapt.moneytor.lib.common.service.MultifactorAuthenticationTypeService;
import com.teamapt.moneytor.lib.common.util.MoneytorUtils;
import com.teamapt.moneytor.lib.corporateinstitution.CorporateInstitutionService;
import com.teamapt.moneytor.lib.transaction.service.TransactionAuthenticationTypeProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Logger;

@Service
// Implement IAsset
public class CardService {

    private final Gson gson = new Gson();

    private final Logger logger = Logger.getLogger(CardService.class.getName());

    private static final String CARD_REQUEST_REDIS_KEY_PREFIX = "card_request_txn:";

    @Value("${moneytor.is.cache.authentication.pin:false}")
    private boolean isCacheAuthPin;

    @Value("${moneytor.redis.pin.transfer.prefix}")
    private String redisPinPrefix;

    @Value("${moneytor.use.pin.for.transfer:false}")
    private boolean isMoneytorUseTransferPin;

    @Value("${moneytor.is.multifactor.authentication.type:true}")
    private boolean isMultifactorAuthenticationType;

    @Autowired
    private CardUtil cardUtil;

    @Autowired
    private MoneytorRedisStore redisStore;

    @Autowired
    private CardRepository cardRepository;

    @Autowired
    private MoneytorAccountService accountService;

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private CardRequestRepository cardRequestRepository;

    @Autowired
    private CardIntegrationService cardIntegrationService;

    @Autowired
    private SystemConfigurationService systemConfigurationService;

    @Autowired
    private CardRequestWorkflowService cardRequestWorkflowService;

    @Autowired
    private CorporateInstitutionService corporateInstitutionService;

    @Autowired
    private CorporateInstitutionRepository corporateInstitutionRepository;

    @Autowired
    private MultifactorAuthenticationTypeService multifactorAuthenticationTypeService;

    @Autowired
    private TransactionAuthenticationTypeProcessor transactionAuthenticationTypeProcessor;

    /**
     * Methods returns a list of active cards
     *
     * @return
     */
    public List<Card> findAll(Principal principal, boolean isActiveCard) throws CosmosServiceException {

        try {
            CosmosUser user = AuthenticationUtils.getPrincipalUser(principal);

            CorporateInstitution corporateInstitution;

            if (user == null)
                throw new CosmosServiceException("Unable to retrieve user details");
            else {
                corporateInstitution = this.corporateInstitutionRepository.
                        findOneByDomainIdAndUsername(user.getAuthenticationDomain().getName(), user.getUsername());

                if (corporateInstitution == null)
                    throw new CosmosServiceException("Unable to get details");
            }

            if (isActiveCard)
                return this.cardRepository.findAllByCardStatusAndCustomerId(CardStatus.ACTIVE,
                        corporateInstitution.getCustomerId());
            else
                return this.cardRepository.findAllByCustomerId(corporateInstitution.getCustomerId());
        } catch (CosmosServiceException ex) {
            throw new CosmosServiceException(ex);
        } catch (Exception ex) {
            throw new CosmosServiceException("An error occurred while processing ", ex);
        }
    }

    /**
     * @param principal
     * @param cardRequestApiModel
     * @return
     * @throws CosmosServiceException
     */
    public CardWorkflowApiModel initiateCardRequest(Principal principal,
                                                    CardRequestApiModel cardRequestApiModel)
            throws CosmosServiceException {

        try {
            /*
          todo check for cases
          1. Has an active card?
          2. Request already made for a card?
           */ // View checks

            // Get corporate institution account
            CorporateInstitutionAccount corporateInstitutionAccount = this.accountService.getAccount(principal,
                    cardRequestApiModel.getSourceAccountProviderCode(), cardRequestApiModel.getSourceAccountNumber());

            // Check if corporate institution account is null
            if (corporateInstitutionAccount == null)
                // Throw exception if corporate institution account is null
                throw new CosmosServiceException("Unable to fetch account details");

            // Check if corporate institution account can be debited
            if (!this.accountService.isDebitableAccout(corporateInstitutionAccount)) {
                logger.info("The account selected for transaction cannot be debited: " +
                        cardRequestApiModel.getSourceAccountNumber());

                // Throw exception if corporate institution account can't be debited
                throw new CosmosServiceException("Sorry, this transaction cannot be completed at the moment. " +
                        "Please contact our One Customer Center");
            }

            // This should be determined before any form of authentication is done
            // Check if pin should be used for transfer
            if (isMoneytorUseTransferPin) { // Still have my doubts...
                // Check if pin is included in the card request
                if (Strings.isNullOrEmpty(cardRequestApiModel.getPin())) {
                    // Throw exception if pin is not included in the request
                    throw new CosmosServiceException("Transaction pin must be included in request");
                }
            }

            if (isCacheAuthPin) {
                logger.info("Setting the Tpx " + cardRequestApiModel.getPin());
                // Cache authentication(transaction) pin
                this.cacheTransactionPin(cardRequestApiModel.getSourceAccountNumber(), cardRequestApiModel.getPin());
            }

            // Authenticate a card request
            this.authenticateCardRequest(principal, cardRequestApiModel);

            // Get a list of authentication types
            List<AuthenticationType> authenticationTypes = this.getNextAuthenticationTypes(cardRequestApiModel,
                    corporateInstitutionAccount.getCurrencyCode());

            // Check if the authentication types returned is null or empty
            if (authenticationTypes == null || authenticationTypes.isEmpty()) {
                return processCardRequest(principal, cardRequestApiModel);
            }

            return processCardRequest(cardRequestApiModel, authenticationTypes);
        } catch (CosmosServiceException ex) {
            throw new CosmosServiceException(ex);
        } catch (Exception ex) {
            throw new CosmosServiceException("An error occurred while processing", ex);
        }
    }

    /**
     * Updates the workflow status of a card request
     *
     * @param corporateInstitution
     * @param cardRequestId
     * @param status
     * @throws CosmosServiceException
     */
    @Transactional
    public void updateCardRequestStatus(CorporateInstitution corporateInstitution, Long cardRequestId, Status status)
            throws CosmosServiceException {

        // Get card request
        CardRequest cardRequest = this.getCardRequest(cardRequestId, corporateInstitution.getId());

        try {
            // Set workflow status on card request
            cardRequest.setWorkflowStatus(status);
            // Save card request
            cardRequestRepository.save(cardRequest);
        } catch (Exception ex) {
            throw new CosmosServiceException(ex.getMessage(), ex);
        }
    }

    /**
     * Returns a list of ids of non-filtered card requests (Workflow status is purely approved)
     *
     * @param corporateInstitution
     * @param cardRequestId
     * @param filterIds
     * @return
     * @throws CosmosServiceException
     */
    public List<Long> cardRequestNotInFilteredList(CorporateInstitution corporateInstitution,
                                                   Long cardRequestId, List<Long> filterIds)
            throws CosmosServiceException {

        // Find the card request
        CardRequest cardRequest = cardRequestRepository.
                findOneByIdAndCorporateInstitutionId(cardRequestId, corporateInstitution.getId());

        List<Long> nonFilteredIdList = new ArrayList<>();

        // Check if card request exists
        if (cardRequest == null)
            // Throw exception if card request doesn't exist
            throw new CosmosServiceException("Card request does not exist");

        // Check if card request workflow status is not declined
        if (!cardRequest.getWorkflowStatus().equals(Status.DECLINED)) {
            if (!filterIds.contains(cardRequest.getId()))
                nonFilteredIdList.add(cardRequest.getId());
        }

        return nonFilteredIdList;
    }

    /**
     * Final or further approval of a card request
     *
     * @param corporateInstitution
     * @param cardRequestId
     * @param approvedCardRequestIds
     * @throws CosmosServiceException
     */
    @Transactional
    public void finalCardRequestApproval(CorporateInstitution corporateInstitution, Long cardRequestId,
                                         List<Long> approvedCardRequestIds)
            throws CosmosServiceException {

        // Get card request
        CardRequest cardRequest = getCardRequest(cardRequestId, corporateInstitution.getId());

        try {
            // Check if card request workflow status is not DECLINED
            if (!cardRequest.getWorkflowStatus().equals(Status.DECLINED)) {
                if (approvedCardRequestIds.contains(cardRequest.getId()))
                    // Set card request workflow status to approved
                    cardRequest.setWorkflowStatus(Status.APPROVED);
                else
                    // Set card request workflow status to declined
                    cardRequest.setWorkflowStatus(Status.DECLINED);
            }

            // Save card request
            cardRequestRepository.save(cardRequest);
        } catch (Exception ex) {
            throw new CosmosServiceException(ex.getMessage(), ex);
        }
    }

    /**
     * Decline card request not in list of approved card requests or if status is pending approval
     *
     * @param principal
     * @param cardRequestId
     * @param approvedCardRequestIds
     * @throws CosmosServiceException
     */
    @Transactional
    public void declineCardRequestNotInList(Principal principal, Long cardRequestId, List<Long> approvedCardRequestIds)
            throws CosmosServiceException {

        // Get corporate institution
        CorporateInstitution corporateInstitution = corporateInstitutionService.getCorporateInstitution(principal);

        // Get card request
        CardRequest cardRequest = getCardRequest(cardRequestId, corporateInstitution.getId());

        try {
            // Check if card request workflow status is DECLINED
            if (!cardRequest.getWorkflowStatus().equals(Status.DECLINED)) {
                // Check if card request id is not in list of approved card requests
                if (!approvedCardRequestIds.contains(cardRequest.getId()))
                    // Set card request workflow status to DECLINED
                    cardRequest.setWorkflowStatus(Status.DECLINED);
            }

            // Check if card request workflow status is PENDING_APPROVAL
            if (!cardRequest.getWorkflowStatus().equals(Status.PENDING_APPROVAL)) {
                // Set card request workflow status to DECLINED
                cardRequest.setWorkflowStatus(Status.DECLINED);
                // Terminate card request workflow
                cardRequestWorkflowService.terminateWorkflow(principal, cardRequest.getId(), null);
            }

            cardRequestRepository.save(cardRequest);
        } catch (Exception ex) {
            throw new CosmosServiceException(ex.getMessage(), ex);
        }
    }

    /**
     * Method gets saved card request
     *
     * @param cardRequestId
     * @param corporateInstitutionId
     * @return
     * @throws CosmosServiceException
     */
    public CardRequest getCardRequest(Long cardRequestId, Long corporateInstitutionId)
            throws CosmosServiceException {

        // Get card request
        CardRequest cardRequest = cardRequestRepository.findOneByIdAndCorporateInstitutionId(
                cardRequestId, corporateInstitutionId);

        // Check if card request exists
        if (cardRequest == null)
            throw new CosmosServiceException("Card request with this identity does not exist");

        return cardRequest;
    }

    /**
     * @param cardRequest
     * @return
     * @throws CosmosServiceException
     */
    public CardRequest processCardRequest(CardRequest cardRequest)
            throws CosmosServiceException {

        if (cardRequest.getWorkflowStatus().equals(Status.APPROVED))
            return cardIntegrationService.processCardRequest(cardRequest);

        return cardRequest;
    }

    /**
     * Method processes a card request
     *
     * @param principal
     * @param cardRequestApiModel
     * @return
     */
    private CardWorkflowApiModel processCardRequest(Principal principal, CardRequestApiModel cardRequestApiModel)
            throws CosmosServiceException {

        try {
            // Get card request initiator
            String cardRequestInitiator = MoneytorUtils.getPrincipalUser(principal);

            // Get corporate institution
            CorporateInstitution corporateInstitution = this.corporateInstitutionService.
                    getCorporateInstitution(principal);

            // Get corporate institution account
            CorporateInstitutionAccount corporateInstitutionAccount = this.cardUtil.getCorporateInstitutionAccount(
                    principal, cardRequestApiModel.getSourceAccountProviderCode(),
                    cardRequestApiModel.getSourceAccountNumber());

            // Check if corporate institution account is null
            if (corporateInstitutionAccount == null)
                // Throw exception if corporate institution account is null
                throw new CosmosServiceException("Unable to fetch account details");

            // Check for valid corporate institution account
            if (!this.accountService.isValidCorporateAccount(principal, cardRequestApiModel.getSourceAccountNumber())) {
                // Throw exception if corporate institution account isn't valid
                throw new CosmosServiceException(ErrorMessages.UNRECOGNIZED_CORPORATE_ACCOUNT);
            }

            // Create an instance of a card request
            CardRequest cardRequest = new CardRequest();

            // Set required properties on instantiated card request
            cardRequest.setCorporateInstitution(corporateInstitution);
            cardRequest.setCustomerId(corporateInstitution.getCustomerId());
            cardRequest.setClientId(cardRequestApiModel.getClientId());
            cardRequest.setCardRequestFee(cardRequestApiModel.getCardRequest().getCardRequestFee());
            cardRequest.setSourceAccountProviderCode(corporateInstitutionAccount.getAccountProviderCode());
            cardRequest.setSourceAccountProviderName(corporateInstitutionAccount.getAccountProviderName());
            cardRequest.setSourceAccountNumber(corporateInstitutionAccount.getAccountNumber());
            cardRequest.setCurrencyCode(corporateInstitutionAccount.getCurrencyCode());
            cardRequest.setInitiator(cardRequestInitiator);
            cardRequest.setTracked(cardRequestApiModel.isTracked());
            cardRequest.setOriginIpAddress(cardRequestApiModel.getOriginIpAddress());
            cardRequest.setNarration(cardRequestApiModel.getNarration());
            cardRequest.setFixedPrice(cardRequestApiModel.getCardRequest().isPriceFixed());
            cardRequest.setWorkflowStatus(Status.PENDING_APPROVAL);
            cardRequest.setCardRequestStatus(CardRequestStatus.REQUEST_REVIEW);
            cardRequest.setCardRequestType(cardRequestApiModel.getCardRequest().getCardRequestType());
            cardRequest.setCardType(CardType.DEBIT);

            // Save the card request
            this.cardRequestRepository.save(cardRequest);

            // Begin card request workflow process
            CardWorkflowApiModel cardWorkflowApiModel = new CardWorkflowApiModel(cardRequestWorkflowService.
                    startWorkflowProcess(cardRequest));

            // Check if card workflow operation or workflow status is null
            if (cardWorkflowApiModel.getOperationStatus() == null || cardWorkflowApiModel.getWorkflowStatus() == null)
                return cardWorkflowApiModel;

            // Check the operation status
            if (cardWorkflowApiModel.getOperationStatus() != null && cardWorkflowApiModel.
                    getOperationStatus().equals(Status.COMPLETED)
                    || cardWorkflowApiModel.getOperationStatus().equals(Status.SUCCESSFUL)
                    || cardWorkflowApiModel.getOperationStatus().equals(Status.APPROVED)
                    && this.isMultifactorAuthenticationType)
                cardWorkflowApiModel.setAuthenticationTypeConfiguration(multifactorAuthenticationTypeService.
                                getAuthenticationTypeForCardRequests(corporateInstitution));

            return cardWorkflowApiModel;
        } catch (CosmosServiceException ex) {
            throw new CosmosServiceException(ex);
        } catch (Exception ex) {
            System.out.println("The card request error is : " + ex.getMessage());
            throw new CosmosServiceException("An error occurred while processing card request", ex);
        }
    }

    /**
     * @param cardRequestApiModel
     * @param authenticationTypes
     * @return
     */
    private CardWorkflowApiModel processCardRequest(CardRequestApiModel cardRequestApiModel,
                                                    List<AuthenticationType> authenticationTypes)
            throws CosmosServiceException {

        try {
            cardRequestApiModel.setAuthenticationTypes(authenticationTypes);
            String transactionId = this.cacheTransaction(cardRequestApiModel);

            return new CardWorkflowApiModel(Status.PENDING, Status.PENDING, transactionId,
                    authenticationTypes.get(0));
        } catch (CosmosServiceException exception) {
            throw new CosmosServiceException(exception);
        }
    }

    /**
     * Method uses the authentication service bean to authenticate a card request
     *
     * @param principal
     * @param cardRequestApiModel
     * @throws CosmosServiceException
     */
    private void authenticateCardRequest(Principal principal, CardRequestApiModel cardRequestApiModel)
            throws CosmosServiceException {

        // Check if request is made from an unknown client
        if (Strings.isNullOrEmpty(cardRequestApiModel.getClientId()))
            // Throw Exception if request is made from an unknown client
            throw new CosmosServiceException("Request made from an unknown client");

        // Extra check to determine a valid corporate account
        if (!this.accountService.isValidCorporateAccount(principal, cardRequestApiModel.getSourceAccountNumber()))
            throw new CosmosServiceException(com.teamapt.moneytor.lib.common.constants.ErrorMessages.
                    UNRECOGNIZED_CORPORATE_ACCOUNT);

        // Authenticate card request
        this.authenticationService.authenticateRequest(principal, cardRequestApiModel.getSourceAccountNumber(),
                cardRequestApiModel);
    }

    /**
     * Method returns a list of authentication types
     *
     * @param cardRequestApiModel
     * @param currencyCode
     * @return
     * @throws CosmosServiceException
     */
    private List<AuthenticationType> getNextAuthenticationTypes(CardRequestApiModel cardRequestApiModel,
                                                                String currencyCode) throws CosmosServiceException {

        //  Validate card request fee
        if (!this.validateCardRequestFee(cardRequestApiModel.getCardRequest().getCardRequestFee()))
            // Throw error if unable to validate card request fee
            throw new CosmosServiceException(ErrorMessages.INVALID_CARD_REQUEST_FEE);

        List<AuthenticationType> authenticationTypes = this.transactionAuthenticationTypeProcessor.
                getRequiredAuthenticationTypes(cardRequestApiModel.getClientId(), currencyCode,
                        cardRequestApiModel.getCardRequest().getCardRequestFee());

        // Return a list of authentication types e.g PIN, OTP etc
        return this.transactionAuthenticationTypeProcessor.getNextAuthenticationTypes(authenticationTypes,
                cardRequestApiModel.getAuthenticationType());
    }

    /**
     * Method performs card request fee validation
     *
     * @param cardRequestFee
     * @return
     * @throws CosmosServiceException
     */
    private boolean validateCardRequestFee(Long cardRequestFee) throws CosmosServiceException {

        // Get the card request fee from systemConfiguration database
        SystemConfiguration systemConfiguration = this.systemConfigurationService.get(
                ConfigurationKey.CARD_REQUEST_FEE);

        // Check if system configuration is null
        if (systemConfiguration == null)
            // Throw exception if system configuration is null
            throw new CosmosServiceException("Invalid card request fee");

        return Objects.equals(cardRequestFee, new Gson().fromJson(systemConfiguration.getValue(), Long.class));
    }

    /**
     * Method caches authentication(transaction) pin
     *
     * @param accountNumber
     * @param pin
     */
    private void cacheTransactionPin(String accountNumber, String pin) {
        String transactionPinKey = redisPinPrefix.concat(accountNumber);

        this.redisStore.remove(transactionPinKey);
        this.redisStore.save(transactionPinKey, pin);
    }

    /**
     *
     * @param cardRequestApiModel
     * @return
     * @throws CosmosServiceException
     */
    private String cacheTransaction(CardRequestApiModel cardRequestApiModel) throws CosmosServiceException {
        try {
            String transactionKey = MoneytorRedisStore.generateTransactionKey(CARD_REQUEST_REDIS_KEY_PREFIX);

            redisStore.save(transactionKey, gson.toJson(cardRequestApiModel));
            return transactionKey;
        } catch (Exception ex) {
            throw new CosmosServiceException("Error storing transaction details for further authentication ", ex);
        }
    }
}
