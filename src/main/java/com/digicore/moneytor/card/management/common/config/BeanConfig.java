package com.digicore.moneytor.card.management.common.config;

import com.digicore.moneytor.card.management.common.constants.CardManagementConstants;
import com.teamapt.api.client.ClientHelper;
import com.teamapt.api.client.CurrentUserOauthTokenProvider;
import com.teamapt.api.provider.HttpHeaderCurrentUserOauthTokenProvider;
import com.teamapt.cosmos.control.lib.service.CosmosTokenService;
import com.teamapt.cosmos.control.lib.service.OauthClientService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfig {

    @Value("${cosmos.control.url}")
    private String cosmosUrl;

    @Bean
    public CosmosTokenService cosmosTokenService() {
        return new CosmosTokenService(cosmosUrl);
    }

    @Bean
    public OauthClientService oauthClientService() {
        return new OauthClientService(cosmosUrl, CardManagementConstants.CLIENT_ID,
                CardManagementConstants.CLIENT_SECRET);
    }

    @Bean
    public CurrentUserOauthTokenProvider currentUserOauthTokenProvider() {
        return new HttpHeaderCurrentUserOauthTokenProvider();
    }

    @Bean
    public ClientHelper clientHelper() {
        ClientHelper clientHelper = new ClientHelper();
        clientHelper.setCurrentUserOauthTokenProvider(currentUserOauthTokenProvider());
        return clientHelper;
    }

}
