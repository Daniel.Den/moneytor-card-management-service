package com.digicore.moneytor.card.management.common.config;

import org.jboss.logging.Logger;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Configures CORS for Card Management Service
 */
@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class CardManagementCORSFilter implements Filter {

    private final Logger logger = Logger.getLogger(CardManagementCORSFilter.class.getName());
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) {

        HttpServletResponse response = (HttpServletResponse) res;
        HttpServletRequest request = (HttpServletRequest) req;

        String originHeader = request.getHeader("Origin");

        response.setHeader("Access-Control-Allow-Origin", originHeader);
        response.setHeader("Access-Control-Allow-Methods", "POST, PUT, GET, OPTIONS, DELETE");
        response.setHeader("Access-Control-Max-Age", "3600");
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setHeader("Access-Control-Allow-Headers", "x-requested-with,origin,accept,content-type,access-control-request-method,access-control-request-headers,authorization,client-id,X-request-encrypted,X-version-number,phoneType");
        response.setHeader("Content-Security-Policy", "default-src https:");
        response.setHeader("Strict-Transport-Security", "max-age=31536000; includeSubDomains");

        if (!"OPTIONS".equals(request.getMethod())) {
            try {
                chain.doFilter(req, res);
            } catch (IOException | ServletException e) {
                logger.debug(e);
            }
        }
    }

    public void init(FilterConfig filterConfig) {}

    public void destroy() {}
}
