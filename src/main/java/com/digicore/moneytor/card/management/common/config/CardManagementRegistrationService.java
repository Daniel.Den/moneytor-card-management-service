package com.digicore.moneytor.card.management.common.config;

import com.digicore.moneytor.card.management.common.constants.CardManagementConstants;
import com.teamapt.cosmos.control.lib.apimodel.OauthClientDetailsApiModel;
import org.springframework.beans.factory.annotation.Value;
import com.teamapt.cosmos.control.lib.service.ClientRegistrationService;

import org.springframework.stereotype.Service;

/**
 * Handles the registration of Merchant Service
 */
@Service
public class CardManagementRegistrationService extends ClientRegistrationService {

    private final static String CLIENT_ID = CardManagementConstants.CLIENT_ID;

    private final static String CLIENT_NAME = CardManagementConstants.CLIENT_NAME;

    private final static String CLIENT_SECRET = CardManagementConstants.CLIENT_SECRET;

    // would need to add other services used
    private final static String RESOURCE_IDS = CardManagementConstants.RESOURCE_IDS;

    private final static String SCOPE = CardManagementConstants.SCOPE;

    private final static String AUTO_APPROVED_SCOPES = CardManagementConstants.AUTO_APPROVED_SCOPES;

    private final static String AUTHORIZED_GRANT_TYPES = CardManagementConstants.AUTHORIZED_GRANT_TYPES;

    private final static String AUTHORITIES = CardManagementConstants.AUTHORITIES;

    private final static Integer ACCESS_TOKEN_VALIDITY_SECONDS = CardManagementConstants.ACCESS_TOKEN_VALIDITY_SECONDS;

    private final static Integer REFRESH_TOKEN_VALIDITY_SECONDS = CardManagementConstants.
            REFRESH_TOKEN_VALIDITY_SECONDS;

    @Value("${moneytor.card.management.oauth.client.redirect_uri}")
    private String WEB_SERVER_REDIRECT_URIS;

    @Override
    public OauthClientDetailsApiModel buildOauthClientDetails() {
        OauthClientDetailsApiModel oauthClientDetails = new OauthClientDetailsApiModel();

        oauthClientDetails.setClientId(CLIENT_ID);
        oauthClientDetails.setClientName(CLIENT_NAME);
        oauthClientDetails.setClientSecret(CLIENT_SECRET);
        oauthClientDetails.setResourceIds(RESOURCE_IDS);
        oauthClientDetails.setScope(SCOPE);
        oauthClientDetails.setWebServerRedirectUris(WEB_SERVER_REDIRECT_URIS);
        oauthClientDetails.setAutoApproveScopes(AUTO_APPROVED_SCOPES);
        oauthClientDetails.setAuthorizedGrantTypes(AUTHORIZED_GRANT_TYPES);
        oauthClientDetails.setAuthorities(AUTHORITIES);
        oauthClientDetails.setAccessTokenValiditySeconds(ACCESS_TOKEN_VALIDITY_SECONDS);
        oauthClientDetails.setRefreshTokenValiditySeconds(REFRESH_TOKEN_VALIDITY_SECONDS);

        return oauthClientDetails;
    }

    public String getClientIDSecret(){
        return CLIENT_ID + ":" + CLIENT_SECRET;
    }
}
